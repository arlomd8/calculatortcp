# CalculatorTCP

Calculator Server Client with TCP Connection. Jaringan Komputer

MEMBER : 
1. Zsalsabilla Pasya Edelani (4210181008)
2. Arlo Mario Dendi (4210181018)
3. Sulthan Mahendra Mubarak (4210181019)

DESCRIPTION : 
A simple calculator to handle multiple client with each their own thread. 
Program will prompt user to input their number and operand.
The server will process each calculation within each client's thread.
