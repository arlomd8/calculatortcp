﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 12000);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;
            while (true)
            {
                Console.Write("First Number : ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Operand : ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Second Number : ");
                s = Console.ReadLine();
                writer.WriteLine(s);
                writer.Flush();

                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);
                Console.WriteLine();
            }
            reader.Close();
            writer.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}