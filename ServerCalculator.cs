﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;


namespace server
{
    public static class globalvar
    {
        public static int counter = 0;
        public static int cliNo = 0;
    }
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {

                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                double result = 0;
                int clientNo = globalvar.cliNo;

                while (true)
                {

                    string n1 = reader.ReadLine();
                    double Num1 = Convert.ToDouble(n1);
                    Console.WriteLine("Client " + clientNo + " >> First Number : " + n1);

                    string op = reader.ReadLine();
                    Console.WriteLine("Client " + clientNo + " >> Operand : " + op);

                    string n2 = reader.ReadLine();
                    double Num2 = Convert.ToDouble(n2);
                    Console.WriteLine("Client " + clientNo + " >> Second Number : " + n2);

                    if (op == "+" || op == "add")
                    {
                        result = Num1 + Num2;
                    }
                    else if (op == "-" || op == "substract")
                    {
                        result = Num1 - Num2;
                    }
                    else if (op == "*" || op == "multiply")
                    {
                        result = Num1 * Num2;
                    }
                    else if (op == "/" || op == "divide")
                    {
                        result = Num1 / Num2;
                    }

                    Console.WriteLine("Client " + clientNo + " >> Results : " + result);
                    string score = Convert.ToString(result);
                    writer.WriteLine("Result : " + score);
                    writer.Flush();
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
                globalvar.counter--;
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }


        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 12000);
                listener.Start();
                Console.WriteLine("Calculator started.");
                Console.WriteLine("Waiting for client connections...");
                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    globalvar.counter++;
                    globalvar.cliNo++;
                    Console.WriteLine(globalvar.counter + " client connected.");
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception n2)
            {
                Console.WriteLine(n2);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}